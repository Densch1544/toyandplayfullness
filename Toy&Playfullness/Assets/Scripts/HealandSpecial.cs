using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace toysandplayfullness
{
    public class HealandSpecial : MonoBehaviour
    {
        public TextMeshProUGUI healUsesText;
        public TextMeshProUGUI specialUsesText;

        public void HealAndSpecialUsesUpdate(Unit unit)
        {
            healUsesText.text = unit.healUses.ToString() + "x";
            specialUsesText.text = unit.specialCharges.ToString();
        }
    }
}
