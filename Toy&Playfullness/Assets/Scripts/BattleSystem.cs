using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

namespace toysandplayfullness
{
    public enum BattleState { START, PLAYERTURN, ENEMYTURN, WON, LOST }

    public class BattleSystem : MonoBehaviour
    {
        public BattleState state;

        public GameObject playerPrefab;
        public GameObject enemyPrefab;

        public Transform playerSpawn;
        public Transform enemySpawn;

        Unit playerUnit;
        Unit enemyUnit;

        public TextMeshProUGUI dialogueText;

        public BattleHUD playerHUD;
        public HealandSpecial playerUses;

        public BattleHUD enemyHUD;

        public Animator playerAnimation;
        public AudioSource attackSound;
        public AudioSource blockSound;
        public AudioSource enemyAttackSound;
        public AudioSource healSound;
        public AudioSource specialSound;

        void Start()
        {
            state = BattleState.START;
            StartCoroutine(SetupBattle());
        }

        IEnumerator SetupBattle()
        {
            GameObject playerGO = Instantiate(playerPrefab, playerSpawn);
            playerUnit = playerGO.GetComponent<Unit>();

            GameObject enemyGO = Instantiate(enemyPrefab, enemySpawn);
            enemyUnit = enemyGO.GetComponent<Unit>();

            dialogueText.text = "A wild " + enemyUnit.unitName + " approaches...";

            playerHUD.SetHUD(playerUnit);
            playerUses.HealAndSpecialUsesUpdate(playerUnit);
            enemyHUD.SetHUD(enemyUnit);

            yield return new WaitForSeconds(2f);

            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }

        void PlayerTurn()
        {
            dialogueText.text = "Choose an action:";
        }        

        public void OnAttackButton()
        {
            if (state != BattleState.PLAYERTURN)
                return;
            attackSound.Play();
            GameObject.FindWithTag("Player").GetComponent<Animations>().Attack();
            StartCoroutine(PlayerAttack());

        }
        
        IEnumerator PlayerAttack()
        {
      
            //playerAnimation.Play("BrobotAttack");
            bool isDead = enemyUnit.TakeDamage(playerUnit.damage);

            dialogueText.text = "The attack is successful!";
            yield return new WaitForSeconds(2f);

            enemyHUD.SetHP(enemyUnit.currentHP);

            if (isDead)
            {
                state = BattleState.WON;
                EndBattle();
            }
            else
            {
                state = BattleState.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }

        public void OnHealButton()
        {
            if (state != BattleState.PLAYERTURN)
                return;
            StartCoroutine(PlayerHeal());
        }

        IEnumerator PlayerHeal()
        {
            bool isHealing = playerUnit.canHeal;

            if(isHealing == false)
            {
                dialogueText.text = "You feel no different...maybe you cant heal anymore?";
            }
            else
            {
            healSound.Play();
            GameObject.FindWithTag("Player").GetComponent<Animations>().Heal();
            playerUnit.Heal(100);
            playerHUD.SetHP(playerUnit.currentHP);
            playerUses.HealAndSpecialUsesUpdate(playerUnit);

            dialogueText.text = "You feel renewed strength!";

            yield return new WaitForSeconds(2f);

            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
            }
        }

        public void OnBlockButton()
        {
            if (state != BattleState.PLAYERTURN)
                return;
            blockSound.Play();
            GameObject.FindWithTag("Player").GetComponent<Animations>().Block();
            StartCoroutine(PlayerBlock());
        }

        IEnumerator PlayerBlock()
        {
            
            dialogueText.text = "You prepare yourself for an Enemy Attack";
            playerUnit.Blocking();

            yield return new WaitForSeconds(2f);

            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }

        public void OnSpecialButton()
        {
            if (state != BattleState.PLAYERTURN)
                return;

            StartCoroutine(PlayerSpecial());
        }

        IEnumerator PlayerSpecial()
        {
            bool canSpecial = playerUnit.canSpecial;
            if (playerUnit.canSpecial == true)
            {

                specialSound.Play();
                GameObject.FindWithTag("Player").GetComponent<Animations>().Special();
                bool isDead = enemyUnit.Special(80);
                playerUnit.UnSpecial();

                enemyHUD.SetHP(enemyUnit.currentHP);
                playerUses.HealAndSpecialUsesUpdate(playerUnit);

                dialogueText.text = "Brobot uses the stored energy for a ULTIMATE ATTACK!";
                
                yield return new WaitForSeconds(2f);

                if (isDead)
                {
                   state = BattleState.WON;
                   EndBattle();
                }
                else
                {
                    state = BattleState.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
            }
            else
            {
                dialogueText.text = "Brobot doesn�t have enough energy! Maybe you can absorb some energy from the enemy?";
            }
        }

        IEnumerator EnemyTurn()
        {
            yield return new WaitForSeconds(2f);

            if (enemyUnit.currentHP < 100 && enemyUnit.healUses > 0)
            {
                healSound.Play();
                GameObject.FindWithTag("Enemy").GetComponent<Animations>().Heal();
                dialogueText.text = enemyUnit.unitName + " heals itself!";
                enemyUnit.Heal(30);

                yield return new WaitForSeconds(2f);

                enemyHUD.SetHP(enemyUnit.currentHP);

                state = BattleState.PLAYERTURN;
                PlayerTurn();
            }
            else
            {
            enemyAttackSound.Play();
            GameObject.FindWithTag("Enemy").GetComponent<Animations>().Attack();
            dialogueText.text = enemyUnit.unitName + " attacks!";

            yield return new WaitForSeconds(2f);

            bool isDead = playerUnit.TakeDamage(enemyUnit.damage);
            playerHUD.SetHP(playerUnit.currentHP);

            bool isBlocking = playerUnit.isBlocking;
            if (isBlocking)
            {
                dialogueText.text = "The attack was partially absorbed!";
                playerUnit.Unblocking();
                playerUses.HealAndSpecialUsesUpdate(playerUnit);
            }            

            yield return new WaitForSeconds(2f);

            if (isDead)
            {
                state = BattleState.LOST;
                EndBattle();                   
                }
            else
            {
                state = BattleState.PLAYERTURN;
                PlayerTurn();
            }            
            }
        }

        void EndBattle()
        {
            if (state == BattleState.WON)
            {
                dialogueText.text = "You won the battle!";               
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            else if (state == BattleState.LOST)
            {
                dialogueText.text = "You were defeated.";
                SceneManager.LoadScene("Menu");
            }
        }
    }
}
