using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace toysandplayfullness
{
    public class Animations : MonoBehaviour
    {
        public GameObject animatedObject;
        public Animator anim;

        public bool player = false;
        public bool sister = false;
        public bool mother = false;
        public bool father = false;

        void Start()
        {
            
            animatedObject.SetActive(true);
            anim = GetComponent<Animator>();
            
        }
        public void Attack()
        {
            if (player == true)
            {
                anim.Play("BrobotAttack");            
            }
            if (sister == true)
            {
                anim.Play("SisAttack");
            }
            if (mother == true)
            {
                anim.Play("MomAttack");
            }
            if (father == true)
            {
                anim.Play("DadAttack");
            }
        }
        public void Block()
        {
            if (player == true)
            {
                anim.Play("BrobotBlock");
            }
        }
        public void Heal()
        {
            if (player == true)
            {
                anim.Play("BrobotHeal");
                
                
            }
            if (sister == true)
            {
                anim.Play("SisHeal");
            }
            if (mother == true)
            {
                anim.Play("MomHeal");
            }
            if (father == true)
            {
                anim.Play("DadHeal");
            }
        }
        public void Special()
        {
            if (player == true)
            {
                anim.Play("BrobotSpecial");
            }
        }
    }
    
}
