using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace toysandplayfullness
{
    public class PlayerFind : MonoBehaviour
    {

        // Dieses Skript ist Obsolete geworden
        public void AttackButtonFind()
        {
            GameObject.FindWithTag("Player").GetComponent<Animations>().Attack();
        }
        public void BlockButtonFind()
        {
            GameObject.FindWithTag("Player").GetComponent<Animations>().Block();
        }
        public void HealButtonFind()
        {
            GameObject.FindWithTag("Player").GetComponent<Animations>().Heal();
            
        }
        public void SpecialButtonFind()
        {
            GameObject.FindWithTag("Player").GetComponent<Animations>().Special();
        }
    }
}
