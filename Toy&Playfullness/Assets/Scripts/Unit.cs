using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace toysandplayfullness
{
    public class Unit : MonoBehaviour
    {
        public string unitName;
        public int damage;
        public int maxHP;
        public int currentHP;
		public bool isBlocking;
		public int healUses = 3;
		public bool canHeal = true;
		public int specialCharges = -3;
		public bool canSpecial = false;

		public void Update()
        {
			if(healUses == 0)
            {
				canHeal = false;
            }
			if(specialCharges == 0)
            {
				canSpecial = true;
            }
        }
		public bool TakeDamage(int dmg)
		{
			if (isBlocking == true)
            {
				currentHP -= dmg;
            }
            else
            {
				currentHP -= dmg;
				currentHP -= dmg;
			}

			if (currentHP <= 0)
			{
				return true;
			}
			else
            {
			return false;
            }        
		}

		public void Heal(int amount)
		{
			if (canHeal == true)
            {
			healUses--;
			currentHP += amount;
			if (currentHP > maxHP)
				currentHP = maxHP;
            }
		}
		public bool Special(int dmg)
        {			
			currentHP -= dmg;

			if (currentHP <= 0)
			{
				return true;
			}
			else
			{
				return false;
			}
            
		}
		public void UnSpecial()
        {
			specialCharges = -3;
			canSpecial = false;
		}

		public void Blocking()
        {
			isBlocking = true;
			
        }

		public void Unblocking()
        {
			isBlocking = false;
			if(specialCharges < 0)
            {
			specialCharges++;
            }
		}
	}
}
